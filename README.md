# Pre- and early post-natal exposure to phthalates and DINCH in a new type of mother-child cohort relying on within-subject pools of repeated urine samples

Git repo for [Philippat et al., 2021](https://doi.org/10.1016/j.envpol.2021.117650)

## Analysis overview

This analysis was performed using [drake](https://github.com/ropensci/drake) workflow management package. 

To run the analysis only the `makefile.R` script needs to be executed. This makefile calls the drake analysis plan (`R/analysis_plan.R`) which outlines the successive analysis steps and calls the different functions used to execute different analysis steps, to be found in the `R/` folder. 

Re-running the analysis also requires an additional `data/` folder not shared here, containing the different data sets. These **data can only be provided upon reasonable request to the SEPAGES steering comity**.

## Session info

```
R version 4.1.1 (2021-08-10)
Platform: x86_64-w64-mingw32/x64 (64-bit)
Running under: Windows 10 x64 (build 18363)

Matrix products: default

locale:
[1] LC_COLLATE=French_France.1252  LC_CTYPE=French_France.1252    LC_MONETARY=French_France.1252 LC_NUMERIC=C                  
[5] LC_TIME=French_France.1252    

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] psychometric_2.2 multilevel_2.6   nlme_3.1-152     ggsci_2.9        ggpubr_0.4.0     trend_1.1.4      pheatmap_1.0.12 
 [8] here_1.0.1       see_0.6.7        cowplot_1.1.1    storr_1.2.5      mice_3.13.0      EnvStats_2.4.0   broom_0.7.9     
[15] MASS_7.3-54      lubridate_1.7.10 forcats_0.5.1    stringr_1.4.0    dplyr_1.0.7      purrr_0.3.4      readr_2.0.1     
[22] tidyr_1.1.3      tibble_3.1.4     ggplot2_3.3.5    tidyverse_1.3.1  labelled_2.8.0   NADA_1.6-1.1     survival_3.2-11 
[29] haven_2.4.3      kableExtra_1.3.4 xlsx_0.6.5       drake_7.13.2    

loaded via a namespace (and not attached):
 [1] colorspace_2.0-2   ggsignif_0.6.2     ellipsis_0.3.2     rio_0.5.27         rprojroot_2.0.2    fs_1.5.0          
 [7] rstudioapi_0.13    fansi_0.5.0        xml2_1.3.2         splines_4.1.1      knitr_1.33         jsonlite_1.7.2    
[13] rJava_1.0-4        dbplyr_2.1.1       compiler_4.1.1     httr_1.4.2         backports_1.2.1    assertthat_0.2.1  
[19] Matrix_1.3-4       fastmap_1.1.0      cli_3.0.1          visNetwork_2.0.9   htmltools_0.5.2    prettyunits_1.1.1 
[25] tools_4.1.1        igraph_1.2.6       gtable_0.3.0       glue_1.4.2         Rcpp_1.0.7         carData_3.0-4     
[31] cellranger_1.1.0   vctrs_0.3.8        svglite_2.0.0      insight_0.14.3     xfun_0.25          xlsxjars_0.6.1    
[37] openxlsx_4.2.4     rvest_1.0.1        lifecycle_1.0.0    rstatix_0.7.0      scales_1.1.1       hms_1.1.0         
[43] parallel_4.1.1     RColorBrewer_1.1-2 yaml_2.2.1         curl_4.3.2         stringi_1.7.4      highr_0.9         
[49] filelock_1.0.2     zip_2.2.0          rlang_0.4.11       pkgconfig_2.0.3    systemfonts_1.0.2  evaluate_0.14     
[55] lattice_0.20-44    htmlwidgets_1.5.3  tidyselect_1.1.1   magrittr_2.0.1     R6_2.5.1           generics_0.1.0    
[61] base64url_1.4      txtq_0.2.4         DBI_1.1.1          pillar_1.6.2       foreign_0.8-81     withr_2.4.2       
[67] abind_1.4-5        performance_0.7.3  modelr_0.1.8       crayon_1.4.1       car_3.0-11         utf8_1.2.2        
[73] tzdb_0.1.2         rmarkdown_2.10     progress_1.2.2     grid_4.1.1         readxl_1.3.1       data.table_1.14.0 
[79] reprex_2.0.1       digest_0.6.27      webshot_0.5.2      extraDistr_1.9.1   munsell_0.5.0      viridisLite_0.4.0 

```
