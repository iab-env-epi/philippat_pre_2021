# All figures for the phthalates descriptive analysis
# M. Rolland
# 17/10/19

# phthalate labels used in plots
phthal_labels <- function(){
  labs <- c("MEP" = "MEP",
            "MnBP" = "MnBP",
            "MiBP" = "MiBP",
            "MBzP" = "MBzP",
            "MEHP" = "MEHP",
            "MEHHP" = "MEHHP",
            "MEOHP" = "MEOHP",
            "MECPP" = "MECPP",
            "MMCHP" = "MMCHP",
            "oh-MPHP" = "oh-\nMPHP",
            "oh-MiNP" = "oh-\nMiNP",
            "oxo-MiNP" = "oxo-\nMiNP",
            "cx-MiNP" = "cx-\nMiNP",
            "oh-MINCH" = "oh-\nMINCH",
            "oxo-MINCH" = "oxo-\nMINCH")
  return(labs)
}

# phthalate levels boxplot
make_levs_bp <- function(phthalates_data_long){
  # compound list
  comp_list <- all_compounds()
  
  # plot data
  plot_data <- phthalates_data_long %>%
    filter(compound %in% comp_list) %>%
    mutate(compound = factor(compound, 
                             levels = comp_list),
           period = fct_recode(period, T2 = "T1"),
           mother_infant = ifelse(period %in% c("T2", "T3"), "Mother", "Infant"),
           mother_infant = factor(mother_infant, levels = c("Mother", "Infant")))
  
  # compound boxplot
  p <- ggplot(plot_data, aes(x = period,
                             y = val_cens,
                             fill = period)) +
    geom_boxplot() +
    geom_hline(aes(yintercept = LOD, linetype = "LOD")) +
    geom_hline(aes(yintercept = LOQ, linetype = "LOQ")) +
    facet_wrap(~compound, ncol = 9,
               labeller = as_labeller(phthal_labels())) +
    scale_y_log10(breaks = c(0.1, 1, 10, 100, 1000, 10000),
                  labels = c("0.1", "1", "10", "100", "1 000", "10 000")) +
    see::theme_modern() +
    theme(axis.text.x = element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.x = element_blank(),
          axis.text.y = element_text(size = 19),
          axis.title = element_text(size = 19),
          axis.ticks.y = element_blank(),
          strip.text = element_text(size = 19, vjust = 0),
          legend.text = element_text(size = 19),
          legend.title = element_blank(),
          legend.position = c(0.83,.3),
          panel.border = element_rect(fill = NA)) +
    scale_fill_manual(values = c("#be29ec", "#efbbff", "royalblue1", "#00c1ff"),
                      labels = c("Pregnancy, second trimester", "Pregnancy, third trimester", 
                                 "Child, two months", "Child, twelve months")) +
    labs(y = expression(paste("Urinary concentrations (", mu, "g/l)"))) +
    guides(fill = guide_legend(order = 1),
           linetype = guide_legend(order = 2))
  
  # save plot without legend
  ggsave("figs/1_phthalates_boxplots.png",
         width = 14, 
         height = 11)
  
  return(p)
}

make_trend_bp <- function(time_trend, phthalates_data_long){
  time_trend <- time_trend %>%
    select(comp, p_preg) %>%
    mutate(stars = 
             case_when(
               abs(p_preg) < 0.001 ~ "***",
               abs(p_preg) < 0.01 ~ "**",
               abs(p_preg) < 0.05 ~ "*",
               TRUE ~ ""
             ),
           lab = case_when(
             comp == "MEP" ~ "MEP",
             comp == "MnBP" ~ "MnBP",
             comp == "MiBP" ~ "MiBP",
             comp == "MBzP" ~ "MBzP",
             comp == "MEHP" ~ "MEHP",
             comp == "MEHHP" ~ "MEHHP",
             comp == "MEOHP" ~ "MEOHP",
             comp == "MECPP" ~ "MECPP",
             comp == "MMCHP" ~ "MMCHP",
             comp == "oh-MPHP" ~ "oh-\nMPHP",
             comp == "oh-MiNP" ~ "oh-\nMiNP",
             comp == "oxo-MiNP" ~ "oxo-\nMiNP",
             comp == "cx-MiNP" ~ "cx-\nMiNP",
             comp == "oh-MINCH" ~ "oh-\nMINCH",
             comp == "oxo-MINCH" ~ "oxo-\nMINCH"),
           lab = str_c(lab, stars, sep = "\n")) %>%
    rename(compound = comp)
  
  labs2 <- time_trend$lab
  names(labs2) <- time_trend$compound
  
  comp_list <- all_compounds()
  
  plot_data <- phthalates_data_long %>%
    left_join(time_trend, by = "compound") %>%
    filter(compound %in% comp_list & 
             !is.na(sample_year) & 
             period %in% c("T1", "T3")) %>%
    mutate(compound = factor(compound, levels = all_compounds()))
  
  p <- ggplot(plot_data, aes(x = factor(sample_year),
                             y = val_cens_sg2,
                             fill = factor(sample_year))) +
    geom_boxplot() +
    facet_wrap(~ compound, ncol = 9, labeller = as_labeller(labs2)) +
    scale_y_log10(breaks = c(0.1, 1, 10, 100, 1000, 10000),
                  labels = c("0.1", "1", "10", "100", "1 000", "10 000")) +
    see::theme_modern() +
    theme(axis.text.x = element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.x = element_blank(),
          axis.text.y = element_text(size = 19),
          axis.title = element_text(size = 19),
          axis.ticks.y = element_blank(),
          strip.text = element_text(size = 19),
          legend.position = c(0.75,.3),
          legend.text = element_text(size = 19),
          legend.title = element_blank(),
          panel.border = element_rect(fill = NA)) +
    scale_fill_manual(labels = c("2014", "2015", "2016", "2017"), 
                      values = c("#50AAC3", "#76C5CC", "#8ED8D2", "#B5F5DB")) +
    geom_hline(aes(yintercept = LOD, linetype = "LOD")) +
    geom_hline(aes(yintercept = LOQ, linetype = "LOQ")) +
    labs(y = expression(paste("Urinary concentrations (", mu, "g/l)")))  +
    guides(fill = guide_legend(order = 1),
           linetype = guide_legend(order = 2))
  
  ggsave("figs/2_time_trend_boxplots.png",
         width = 14, 
         height = 11)
  
  return(p)
}

# trend boxplot for molar sums
make_trend_bp_ms <- function(time_trend, phthalates_data_long){
  time_trend <- time_trend %>%
    select(comp, p_preg) %>%
    mutate(comp = as.character(comp)) %>%
    filter(str_detect(comp, "\\*")) %>%
    mutate(stars = 
             case_when(
               abs(p_preg) < 0.001 ~ "***",
               abs(p_preg) < 0.01 ~ "**",
               abs(p_preg) < 0.05 ~ "*",
               TRUE ~ ""
             ),
           lab = case_when(
             comp == "DiNP*" ~ "DiNP",
             comp == "DINCH*" ~ "DINCH",
             comp == "DEHP*" ~ "DEHP"
             ),
           lab = str_c(lab, stars, sep = "\n")) %>%
    rename(compound = comp)
  
  labs2 <- time_trend$lab
  names(labs2) <- time_trend$compound
  
  comp_list <- c("DiNP*", "DINCH*", "DEHP*")
  
  plot_data <- phthalates_data_long %>%
    left_join(time_trend, by = "compound") %>%
    filter(compound %in% comp_list & 
             !is.na(sample_year) & 
             period %in% c("T1", "T3")) %>%
    mutate(compound = factor(compound, levels = comp_list))
  
  p <- ggplot(plot_data, aes(x = factor(sample_year),
                             y = val,
                             fill = factor(sample_year))) +
    geom_boxplot() +
    facet_wrap(~ compound, ncol = 9, labeller = as_labeller(labs2)) +
    scale_y_log10(breaks = c(0.1, 1, 10, 100, 1000, 10000),
                  labels = c("0.1", "1", "10", "100", "1 000", "10 000")) +
    see::theme_modern() +
    theme(axis.text.x = element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.x = element_blank(),
          axis.text.y = element_text(size = 19),
          axis.title = element_text(size = 19),
          axis.ticks.y = element_blank(),
          strip.text = element_text(size = 19),
          legend.text = element_text(size = 19),
          legend.title = element_blank(),
          panel.border = element_rect(fill = NA)) +
    scale_fill_manual(labels = c("2014", "2015", "2016", "2017"), 
                      values = c("#50AAC3", "#76C5CC", "#8ED8D2", "#B5F5DB")) +
    labs(y = expression(paste("Molar sum (", mu, "mol/l)")))  +
    guides(fill = guide_legend(order = 1),
           linetype = guide_legend(order = 2))
  
  ggsave("figs/2_time_trend_boxplots.png",
         width = 14, 
         height = 11)
  
  return(p)
}

corr_plot <- function(period_correlation){
  # set corr long format
  corr_long <- period_correlation %>%
    select(-starts_with("n")) %>%
    pivot_longer(cols = -compound,
                 names_to = "periods",
                 values_to = "corr") %>%
    mutate(periods = str_remove(periods, "cor_"))
  
  # prepare plot data
  plot_data <- corr_long %>%
    filter(periods != "preg_pn") %>%
    mutate(compound = factor(compound, levels = all_compounds2()),
           compound = fct_rev(compound),
           periods = factor(periods, levels = c("t1_t3", "m2_y1", "preg_m2", "preg_y1")),
           periods = fct_recode(periods, 
                                "T2 - T3" = "t1_t3",
                                "M2 - M12" = "m2_y1",
                                "Mother - M2" = "preg_m2",
                                "Mother - M12" = "preg_y1"))
  
  # produce plot
  p <- ggdotchart(plot_data,
                  group = "compound",
                  x = "compound",
                  y = "corr",
                  ylab = "Spearman correlation",
                  color = "periods",
                  shape = "periods",
                  palette = "jco",
                  rotate = TRUE,
                  dot.size = 3) +
    geom_hline(yintercept = 0) +
    theme_cleveland() +
    theme(legend.title = element_blank())
  
  # save plot
  ggsave("figs/3_correlations_dotchart.png",
         width = 14,
         height = 12,
         units = "cm")
  
  return(p)
}

make_heatmap <- function(cor_tab_preg, cor_tab_m2, cor_tab_y1){
  col <- colorRampPalette(c("blue", "white", "red"))(20)
  
  # remove SG
  cor_tab_preg_2 <- cor_tab_preg[-nrow(cor_tab_preg), -ncol(cor_tab_preg)]
  cor_tab_m2_2 <- cor_tab_m2[-nrow(cor_tab_m2), -ncol(cor_tab_m2)]
  cor_tab_y1_2 <- cor_tab_y1[-nrow(cor_tab_y1), -ncol(cor_tab_y1)]
  
  p1 <- pheatmap(as.matrix(cor_tab_preg_2), 
                 treeheight_row = 0, 
                 treeheight_col = 0, 
                 border_color = NA,
                 legend = FALSE,
                 cluster_rows = FALSE,
                 cluster_cols = FALSE,
                 main = "a. Pregnancy samples")
  
  p2 <- pheatmap(as.matrix(cor_tab_m2_2), 
                 treeheight_row = 0, 
                 treeheight_col = 0, 
                 border_color = NA,
                 cluster_rows = FALSE,
                 cluster_cols = FALSE,
                 main = "b. Child, two-month samples")
  
  p3 <- pheatmap(as.matrix(cor_tab_y1_2), 
                 treeheight_row = 0, 
                 treeheight_col = 0, 
                 border_color = NA,
                 legend = FALSE,
                 cluster_rows = FALSE,
                 cluster_cols = FALSE,
                 main = "c. Child, twelve-month samples")
  
  p <- cowplot::plot_grid(p1$gtable, p2$gtable, p3$gtable)
  
  ggsave("figs/4_compound_heatmaps.png", 
         height = 7, 
         width = 7, 
         dpi = 300)
  
  return(p)
}

preg_cohort_comp <- function(cohorts_preg, cohorts_preg_desc){
  
  # set coort table to long format
  cohorts_preg_long <- cohorts_preg %>%
    pivot_longer(cols = -compound,
                 names_to = "cohort",
                 values_to = "val") %>%
    mutate(period = "preg",
           compound = factor(compound, levels = all_compounds()))
  
  # add new line to long cohort lab
  cohorts_preg_desc <- cohorts_preg_desc %>%
    mutate(
      cohort_lab = case_when(
        cohort == "SEPAGES_T1" ~ "SEPAGES (France, 2014-2017)\nSecond Trimester",
        cohort == "SEPAGES_T3" ~ "SEPAGES (France, 2014-2017)\nThird Trimester",
        TRUE ~ cohort_lab
      ),
      alpha = ifelse(str_detect(cohort, "SEPAGES"), "SEPAGES", "OTHERS")
    )
  
  # add cohort info
  cohorts_preg_long <- cohorts_preg_long %>%
    left_join(cohorts_preg_desc, by = "cohort")
  
  # plot
  p <- ggplot(cohorts_preg_long, aes(x = cohort,
                                     y = val,
                                     fill = cohort_lab,
                                     alpha = alpha)) +
    geom_histogram(stat = "identity", ) +
    facet_wrap(~compound, 
               labeller = as_labeller(phthal_labels()),
               ncol = 9) +
    see::scale_fill_material_d()+
    theme(text = element_text(size = 19),
          strip.text = element_text(size = 19, vjust = 0, face = "bold"),
          legend.text = element_text(size = 19),
          axis.text.y = element_text(size = 19),
          axis.title.x = element_blank(),
          axis.text.x = element_blank(),
          axis.ticks.x = element_blank(),
          strip.background = element_rect(fill = "white"),
          panel.background = element_rect(fill = "white"),
          panel.grid.major.y = element_line(color = "black", linetype = "dotted"),
          panel.border = element_rect(fill = NA),
          legend.position = c(0.95, 0.3),
          legend.title = element_blank(),
          plot.margin = margin(0, 6, 0, 2, "cm")) +
    guides(fill = guide_legend(override.aes= list(alpha = c(0.3, 0.3, 0.3, 0.3, 1, 1))),
           alpha = "none") +
    labs(y = expression(Reported~median~concentration~(mu~g/l))) +
    scale_alpha_manual(values = c(0.3, 1),
                       labels = c("OTHER", "SEPAGES"))
  ggsave("figs/5_pregnancy_cohorts.png",
         width = 14, 
         height = 11)
  
  return(p)
}


pn_cohort_comp <- function(cohorts_pn, cohorts_pn_desc){
  
  # set coort table to long format
  cohorts_pn_long <- cohorts_pn %>%
    pivot_longer(cols = -compound,
                 names_to = "cohort",
                 values_to = "val") %>%
    mutate(period = "preg",
           compound = factor(compound, levels = all_compounds()),
           val = ifelse(val == 0, 0.1, val))
  
  # add cohort info
  cohorts_pn_long <- cohorts_pn_long %>%
    left_join(cohorts_pn_desc, by = "cohort") %>%
    mutate(
      cohort = factor(cohort, 
                      levels = c( "KIM_3M", "VOLKEL_1_5M", 
                                  "NAVARANJAN_3M", "ARBUCKLE_2_3M", 
                                  "CARLSTEDT_2_6M", "SEPAGES_M2", 
                                  "SEPAGES_M12", "KIM_12M", "WATKINS_12M", 
                                  "NAVARANJAN_12M", "FROMME_15_21M")),
      cohort_order = as.numeric(cohort),
      cohort_lab = str_replace(cohort_lab, "et al ", "et al ("),
      cohort_lab = str_replace(cohort_lab, "SEPAGES ", "SEPAGES ("),
      cohort_lab = str_replace(cohort_lab, "months", "months,"),
      cohort_lab = str_c(cohort_lab, ")"),
      cohort_lab = factor(cohort_lab),
      cohort_lab = fct_reorder(cohort_lab, cohort_order),
      alpha = ifelse(str_detect(cohort, "SEPAGES"), "SEPAGES", "OTHERS"))
  
  # plot
  p <- ggplot(cohorts_pn_long, aes(x = cohort_lab,
                                   y = val,
                                   fill = cohort_lab,
                                   alpha = alpha)) +
    geom_histogram(stat = "identity") +
    facet_wrap(~compound, 
               labeller = as_labeller(phthal_labels()),
               ncol = 9) +
    see::scale_fill_material_d() +
    theme(text = element_text(size = 19),
          strip.text = element_text(size = 19, vjust = 0, face = "bold"),
          legend.text = element_text(size = 19),
          axis.text.y = element_text(size = 19),
          axis.title.x = element_blank(),
          axis.text.x = element_blank(),
          axis.ticks.x = element_blank(),
          strip.background = element_rect(fill = "white"),
          panel.background = element_rect(fill = "white"),
          panel.grid.major.y = element_line(color = "black", linetype = "dotted"),
          panel.border = element_rect(fill = NA),
          legend.position = c(0.95, 0.3),
          legend.title = element_blank(),
          plot.margin = margin(0, 6, 0, 2, "cm")) +
    guides(fill = guide_legend(override.aes= list(alpha = c(0.3, 0.3, 0.3, 0.3, 0.3, 1, 1, 0.3, 0.3, 0.3, 0.3))),
           alpha = "none") +
    labs(y = expression(Reported~median~concentration~(mu~g/l))) +
    scale_alpha_manual(values = c(0.3, 1),
                       labels = c("OTHER", "SEPAGES"))
  
  ggsave("figs/6_post_nat_cohorts.png",
         width = 14, 
         height = 11)
  
  return(p)
}

# prepare all ratios boxplots
prepare_ratios_boxplots <- function(phthalates_data){
  p <- phthalates_data %>%
    pivot_longer(
      cols = starts_with("r"),
      names_to = "r",
      values_to = "ratio"
    ) %>%
    mutate(
      period = factor(period, levels = c("T1", "T3", "M2", "Y1")),
      r = fct_recode(r, 
                     "MEHP /\n(MEOHP + MECPP +\nMEHHP + MMCHP)" = "r0",
                     "MEHHP / MEHP" = "r1",
                     "MECPP / MEHP" = "r2",
                     "MEOHP / MEHHP" = "r3",
                     "MECPP / MEOHP" = "r4",
                     "oxo-MinP /\noh-MinP" = "r5",
                     "cx-MiNP /\noxo-MinP" = "r6",
                     "oh-MINCH /\noxo-MINCH" = "r7",
                     "MMCHP / MEHP" = "r8",
                     "MMCHP / MEOHP" = "r9",
                     "MMCHP / MECPP" = "r10")
    ) %>%
    ggplot(aes(y = ratio,
               x = period)) +
    geom_boxplot(fill = "gray90") +
    facet_wrap(~r, scales = "free", nrow = 2) +
    scale_y_log10() +
    see::theme_lucid() +
    theme(legend.position = "none",
          axis.title.x = element_blank(),
          strip.text = element_text(size = 8))
  
  ggsave("figs/ratios_bloxplots.png",
         scale = 1/90,
         width = 850,
         height = 500)
  
  return(p)
}